# OpenML dataset: Red--White-wine-Dataset

https://www.openml.org/d/43620

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Business Problem
It is often said that great wine is made in the vineyard, not in the winery.  However, winemakers have the ability to modify certain aspects of the wines they produce, such as the level of acidity, sweetness or alcohol, as well as the shelf life.  But which aspects should winemakers modify to positively impact consumers perceptions and/or sales of their wines?
Unicorn Winery has hired Digitas Advanced Analytics to help the organization better understand the relationships between certain physiochemical properties and the perceived quality of wines so that its winemaking team can make more informed decisions during production. A Digitas AA analyst already obtained data from a third-party industry organization and started an exploratory analysis.  However, she recently was assigned to another project and will not be able to finish the work for Unicorn Winery herself.
Your Objective
Your objective is to collaborate with your Digitas supervisor to continue the analysis and to provide Unicorn Winery with insights on how to maximize the appeal of its wines.  Its also looking for ideas on other analyses it might conduct in the future to support the business, as well as what data would be required to run them.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43620) of an [OpenML dataset](https://www.openml.org/d/43620). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43620/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43620/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43620/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

